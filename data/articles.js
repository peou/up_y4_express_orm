const articles = [
  {
    id: 1,
    title: "Hello World",
    contents: "Welcome to learning Express!",
    created_by: "User1",
    is_published: true,
    created_at: "2023-05-01",
    updated_at: "2023-05-01",
  },
];
module.exports = articles;